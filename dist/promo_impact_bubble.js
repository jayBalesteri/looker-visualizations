const chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };
  
  const applyCustomLabel = (tooltipItem, data) => {
    console.log('tooltipItem: ', tooltipItem);
    console.log('data: ', data);
    let item = data['datasets'][tooltipItem.datasetIndex]['data'][tooltipItem['index']];
    console.log('item:: ', item);
    return `Promotion: ${item.id}`;
  }
  
  looker.plugins.visualizations.add({
    // Id and Label are legacy properties that no longer have any function besides documenting
    // what the visualization used to have. The properties are now set via the manifest
    // form within the admin/visualizations page of Looker
    id: "revionics_bubble",
    label: "Revionics Bubble",
    options: {
      font_size: {
        type: "string",
        label: "Font Size",
        values: [
          {"Large": "large"},
          {"Small": "small"}
        ],
        display: "radio",
        default: "large"
      }
    },
    // Set up the initial state of the visualization
    create: function(element, config) {
      element.innerHTML = `
        <style>
          #container {
            /* Vertical centering */
            position: relative;
            margin: 20px;
            height: 100%;
            /* display: flex; */
            /* flex-direction: column; */
            /* justify-content: center; */
            text-align: center;
          }
        </style>
      `;
      // Create a container element to let us center the text.
      var container = element.appendChild(document.createElement("div"));
      container.id = "container";
  
      this._canvas = container.appendChild(document.createElement("canvas"));
      this._canvas.id = "canvas";
    },
    // Render in response to the data or settings changing
    updateAsync: function(data, element, config, queryResponse, details, done) {
      this.clearErrors(); // Clear any errors from previous updates
      // // Throw some errors and exit if the shape of the data isn't what this chart needs
      // if (queryResponse.fields.dimensions.length == 0) {
      //   this.addError({title: "No Dimensions", message: "This chart requires dimensions."});
      //   return;
      // }
  
      // show ALL data:
      // console.log('data:: ', data);
      // console.log('data length: ', data.length);
      // console.log('queryResponse: ', queryResponse);
  
      let dataSets = {
        keepDoing: [],
        findFunding: [],
        changeOffers: [],
        stopDoing: [],
        reallocateFunds: [],
        notApplicable: []
      }
  
      function evaluateItem(item) {
        let current = {
          id: Object.entries(item)[0][1].value, // promotion id
          x: Object.entries(item)[1][1].value, // promotion impact
          y: Object.entries(item)[2][1].value, // revenue impact
          r: Math.floor(Math.random() * 5) // TODO: radius of circle
        }
  
        switch (true) {
          case current.y > 0 && current.x > 0:
            dataSets.keepDoing.push(current);
            break;
          case current.y < 0 && (current.y >= current.x * -1):
            dataSets.findFunding.push(current);
            break;
          case  current.y < 0 && (current.y < current.x * -1):
            dataSets.changeOffers.push(current);
            break;
          case current.y < 0 && current.x < 0:
            dataSets.stopDoing.push(current);
            break;
          case current.x <= 0 && current.y >= 0:
            dataSets.reallocateFunds.push(current);
            break;
          default:
            dataSets.notApplicable.push(current);
            break;
        }
      }
  
      data.forEach(d => evaluateItem(d));
      console.log('new dataSets:: ', dataSets);
  
  
      // console.log('dataSet: ', dataSet());
      var DEFAULT_DATASET_SIZE = data.length;
  
      var addedCount = 0;
      var color = Chart.helpers.color;
      var bubbleChartData = {
        datasets: [{
          label: 'Keep Doing',
          backgroundColor: color(chartColors.blue).alpha(0.5).rgbString(),
          borderColor: chartColors.blue,
          borderWidth: 1,
          data: dataSets.keepDoing
        }, {
          label: 'Find Funding',
          backgroundColor: color(chartColors.green).alpha(0.5).rgbString(),
          borderColor: chartColors.green,
          borderWidth: 1,
          data: dataSets.findFunding
        }, {
          label: 'Change Offers',
          backgroundColor: color(chartColors.yellow).alpha(0.5).rgbString(),
          borderColor: chartColors.yellow,
          borderWidth: 1,
          data: dataSets.changeOffers
        }, {
          label: 'Stop Doing',
          backgroundColor: color(chartColors.red).alpha(0.5).rgbString(),
          borderColor: chartColors.red,
          borderWidth: 1,
          data: dataSets.stopDoing
        }, {
          label: 'Reallocate Funds',
          backgroundColor: color(chartColors.purple).alpha(0.5).rgbString(),
          borderColor: chartColors.purple,
          borderWidth: 1,
          data: dataSets.reallocateFunds
        }, {
          label: 'Not Applicable',
          backgroundColor: color(chartColors.grey).alpha(0.5).rgbString(),
          borderColor: chartColors.grey,
          borderWidth: 1,
          data: dataSets.notApplicable
        }]
      };
  
      var ctx = document.getElementById('canvas').getContext('2d');
      window.myChart = new Chart(ctx, {
        type: 'bubble',
        data: bubbleChartData,
        options: {
          responsive: true,
          maintainAspectRatio: true,
          aspectRatio: 2,
          legend: {
            display: true
          },
          plugins: {
            title: {
              display: false,
              text: 'Bubble Chart'
            },
          },
          tooltips: {
            // Disable the on-canvas tooltip
            enabled: false,
  
            custom: function(tooltipModel) {
                // Tooltip Element
                var tooltipEl = document.getElementById('chartjs-tooltip');
  
                // Create element on first render
                if (!tooltipEl) {
                    tooltipEl = document.createElement('div');
                    tooltipEl.id = 'chartjs-tooltip';
                    tooltipEl.innerHTML = '<table></table>';
                    document.body.appendChild(tooltipEl);
                }
  
                // Hide if no tooltip
                if (tooltipModel.opacity === 0) {
                    tooltipEl.style.opacity = 0;
                    return;
                }
  
                // Set caret Position
                tooltipEl.classList.remove('above', 'below', 'no-transform');
                if (tooltipModel.yAlign) {
                    tooltipEl.classList.add(tooltipModel.yAlign);
                } else {
                    tooltipEl.classList.add('no-transform');
                }
  
                function getBody(bodyItem) {
                    return bodyItem.lines;
                }
  
                // Set Text
                if (tooltipModel.body) {
                    var titleLines = tooltipModel.title || [];
                    var bodyLines = tooltipModel.body.map(getBody);
  
                    var innerHtml = '<thead>';
  
                    titleLines.forEach(function(title) {
                        innerHtml += '<tr><th>' + title + '</th></tr>';
                    });
                    innerHtml += '</thead><tbody>';
  
                    bodyLines.forEach(function(body, i) {
                        var colors = tooltipModel.labelColors[i];
                        var style = 'background: #fff';
                        style += '; border-color: magenta';
                        style += '; border-width: 2px';
                        var span = '<span style="' + style + '"></span>';
                        innerHtml += '<tr><td>' + span + body + '</td></tr>';
                    });
                    innerHtml += '</tbody>';
  
                    var tableRoot = tooltipEl.querySelector('table');
                    tableRoot.innerHTML = innerHtml;
                }
  
                // `this` will be the overall tooltip
                var position = this._chart.canvas.getBoundingClientRect();
  
                // Display, position, and set styles for font
                tooltipEl.style.opacity = 1;
                tooltipEl.style.position = 'absolute';
                tooltipEl.style.left = position.left + window.pageXOffset + tooltipModel.caretX + 'px';
                tooltipEl.style.top = position.top + window.pageYOffset + tooltipModel.caretY + 'px';
                tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
                tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
                tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
                tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                tooltipEl.style.pointerEvents = 'none';
            }
          }
        }
      });
      var colorNames = Object.keys(chartColors);
  
      // We are done rendering! Let Looker know.
      done()
    }
  });